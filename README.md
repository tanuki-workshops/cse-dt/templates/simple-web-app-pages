# simple-web-app-pages

## Development

### Build and run the application

```bash
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" . 
./build.sh
./serve.sh
```

- The project must be public
- The Pages are available for everyone
